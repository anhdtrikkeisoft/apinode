var express = require("express");
var app = express();
var server = require("http").createServer(app);


app.get('/', function  (req, res){
    console.log('Get request')
    res.end();
})

// listen
server.listen(process.env.port || 4000, function () {
    console.log("now listening for request")
});